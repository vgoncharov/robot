import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.*;

public class Test {
    public static void main(String... args) {
        myFrame okno = new myFrame();
    }
}

class myFrame extends JFrame {

    private Robot rob;
    private Timer tm;
    private int kol = 0;
    private Frame wnd;

    public myFrame() {

        try {
            rob = new Robot();
        } catch (Exception e) {
            e.printStackTrace();
        }

        tm = new Timer(5000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveScreen();
                System.out.println("снимаем скрин экрана");
            }
        });

        tm.start();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setVisible(false);
    }

    private void saveScreen() {
        kol++;

        Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
        int w = dm.width;
        int h = dm.height;

        if (kol == 5) {
            tm.stop();
            wnd = new Frame();
            wnd.setResizable(false);
            wnd.setBounds(0,0, w, h);
            wnd.setBackground(Color.RED);
            wnd.setAlwaysOnTop(true);
            wnd.setUndecorated(true);
            wnd.setOpacity(0.5f);
            wnd.setVisible(true);
        }

        try {
            BufferedImage img = rob.createScreenCapture(new Rectangle(0, 0, w, h));
            ImageIO.write(img, "PNG", new File("D:\\screen\\"+kol+".png"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}